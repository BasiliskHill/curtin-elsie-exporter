import axios from "axios";
import fs from "fs";
import process from "process";
import input from "@inquirer/input";
import password  from "@inquirer/password";
import checkbox from "@inquirer/checkbox";

const modulesPath = "./modules/";

console.log("Loading modules");

// Attempt to read modules directory
let modulesDir = [];
try {
  modulesDir = fs.readdirSync(modulesPath);
} catch (err) {
  if (err.code == "ENOENT") {
    console.error("Failed while reading modules directory, exiting");
    process.exit(1);
  } else throw err;
}

if (modulesDir.length == 0) {
  console.error("No modules found, exiting");
  process.exit(1);
}

// Attempt to import modules
let modules = [];

for (let i = 0; i < modulesDir.length; i++) {
  const module = modulesDir[i];
  const {execute, name} = await import(`${modulesPath}${module}`);

  if (execute && name) {
    modules = modules.concat([{execute, name}]);
    console.log(`Sucessfully loaded '${name}' module`)
  } else {
    console.warn(`Invalid module file: '${module}'`);
  }
}
console.log();

// Authenticate with Oasis
const curtinId = await input({message: "Enter your student ID"});
const curtinPassword = await password({message: "Enter your password", mask: true})
const authResponse = await axios.post("https://elsie.curtin.edu.au/api/sessions", {
  curtinId,
  password: curtinPassword
});

// Get activities data
const bearer = authResponse.data.data.token;
const cookie = authResponse.headers['set-cookie'][0].split(';')[0];
axios.defaults.headers.common['Authorization'] = `BEARER ${bearer}`;
const activityResponse = await axios.get(`https://elsie.curtin.edu.au/api/students/${curtinId}/study-activities`, {
      responseType: 'json',
      headers: {
          'Accept': '*/*',
          Connection: 'keep-alive',
          'Keep-Alive': 'timeout=1500, max=100',
          'Content-Type': 'application/json',
          'Accept-Encoding': 'gzip, deflate, br',
          'Cookie': cookie
      }})
const activitesData = activityResponse.data.data;

// Run modules
const modulesToRun = await checkbox({message: "Select the modules you wish to run", choices: modules.map((module, i) => ({name: module.name, value: i}))})
modulesToRun.forEach((i) => modules[i].execute(activitesData));