# Curtin Elsie Extractor
Curtin Elsie Extractor is a modular platform for extracting the class timetable from Curtin's [Elsie](elsie.curtin.edu.au), producing usable files for other platforms that are more accessible for planning an individual's schedule.

## Platforms
The service currently supports exporting to:
- Calendar format (.ics)
- Todoist project templates (.csv)

### Planned
- Update todoist to allow modifying due dates
- Update modules to allow changing of output directories / names

## Installation
- Clone or download the zip using the Code menu above and extract
- Install [NodeJS](https://nodejs.org/en/download/current)
- Install dependencies using `npm i`

## Usage
- Run using `node main.mjs`
- Follow the prompts in the terminal