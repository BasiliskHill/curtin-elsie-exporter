import fs from "fs";
import ics from "ics"; 
import dateFns from "date-fns";

export const name = "Calendar"

export const execute = (activitesData) => {
    const activities = activitesData.map((activity) => {
        const startDate = new Date(activity.startDateTime);
        const endDate = new Date(activity.endDateTime);
      
        return {
          start: [startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate(), startDate.getHours(), startDate.getMinutes()],
          duration: {hours: dateFns.differenceInHours(endDate, startDate), minutes: dateFns.differenceInMinutes(endDate, startDate) % 60},
          title: `${activity.unit.unitCode} - ${activity.activityName}`,
          description: `${activity.unit.fullTitle} - ${activity.activityName} - ${activity.activityType}`,
          location: `${activity.location.buildingNumber}.${activity.location.roomNumber} - ${activity.location.name}`,
        }
      
    })
      
    const { error, value } = ics.createEvents(activities)
    
    if (error) {
    console.log(error)
    } else {
    fs.writeFileSync("./outCalendar.ics", value);
    }
}
