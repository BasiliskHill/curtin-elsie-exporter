import fs from 'fs';
import _ from 'lodash';
import dateFns from 'date-fns';

export const name = "Todoist";

export const execute = (activitesData) => {
    const units = {};

    // Defaults
    const outputDir = "./outTodoist/";

    // Defaults for todoist csv
    const description = "";
    const priority = 4;
    const indent = 1;
    const author = "";
    const responsible = "";
    const dateLang = "";
    const timezone = "";
    const durationUnit = "minute";

    const header = "TYPE,CONTENT,DESCRIPTION,PRIORITY,INDENT,AUTHOR,RESPONSIBLE,DATE,DATE_LANG,TIMEZONE,DURATION,DURATION_UNIT\nmeta,view_style=board,,,,,,,,,,";
    const seperator = ",,,,,,,,,,,\n";

    // Create units object with all necessary class data; (UNIT -> CLASS TYPE -> CLASS)
    activitesData.forEach((activity) => {
        const unitActivity = `[${activity.unit.unitCode}][${activity.activityType}]`;

        const date = (new Date(activity.startDateTime)).toDateString();
        const duration = dateFns.differenceInMinutes(new Date(activity.endDateTime), new Date(activity.startDateTime));

        _.set(units, `${unitActivity}[${activity.activityType} ${Object.keys(_.get(units, unitActivity, {})).length + 1}]`, {
            description,
            priority,
            indent,
            author,
            responsible,
            date,
            dateLang,
            timezone,
            duration,
            durationUnit
        });
    });

    // Ensure output directory exists
    fs.mkdirSync(outputDir, {recursive: true});

    // Make output file for each unit
    Object.entries(units).forEach((unit) => {
        const file = `${outputDir}${unit[0]}.csv`;
        fs.writeFileSync(file, `${header}\n${seperator}`);

        Object.entries(unit[1]).forEach((section) => {
            fs.appendFileSync(file, `section,${section[0]},,,,,,,,,,\n`);

            Object.entries(section[1]).forEach((activity) => {
                const {
                    description,
                    priority,
                    indent,
                    author,
                    responsible,
                    date,
                    dateLang,
                    timezone,
                    duration,
                    durationUnit
                } = activity[1];
                const content = activity[0];

                fs.appendFileSync(file, `task,${content},${description},${priority},${indent},${author},${responsible},${date},${dateLang},${timezone},${duration},${durationUnit}\n`);
            });

            fs.appendFileSync(file, seperator);
        });
    });
};